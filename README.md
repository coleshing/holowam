# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* A modified ROSBridgeLib by Mathias Ciarlo, to be used with Barrett WAM in both Hololens and any other Unity supported devices such as Occulus Rift
* Version 0.2 

### How do I get set up? ###

* To setup this project copy all the files inside the Assets folder to the Unity project Assets folder
* Open the ROSscene, which has the the following
* WAM prefab with the WAMViewer script (check the use_sliders to allow the sliders to control the WAM)
* Hololenscamera, DefaultCursor object which are taken from the HoloToolKit Unity package (https://github.com/Microsoft/HoloToolkit-Unity)
* Managers with all the default manager scripts as instructed here https://github.com/Microsoft/HoloToolkit-Unity/blob/master/GettingStarted.md

*Make sure the computer on Ubuntu that will control the WAM has the WAM node working following from the Barrett Support site http://support.barrett.com/wiki/WAM/InstallBarrettRosPkg
* Also make sure rosbridge suite is installed. http://wiki.ros.org/rosbridge_suite
* Edit the WAM Viewer script ip address to match the WAM computer
### To Launch ###
* First run the wam_node using roslaunch wam_node wam_node.launch then do the shift activate
* Then run the rosbridge server by roslaunch rosbridge_server rosbridge_websocket.launch
* Then click play on Unity Editor to see the WAM mimic the physical WAM
* Control the WAM either using a rostopic pub message from another program or use the sliders to see both the virtual and physical WAM move

* To run on Hololens, just build the system to windows store or the default hololens setup and then deploy as normal

###Package Contents ###

* BHandSim - the barrett hand sim prefab and related scripts, including service class and subscribers
* HoloToolKit - is provided by Microsoft, to update copy the latest relase on their holotool kit sdk
* ListConversion - a class with functions to convert to arrays for the messages
* Materials - materials used by the wam and barrett hand, needs to be cleaned up and sorted into their folders
* moveit - the moveit related custom package, contains its serivce class and a viewer that controls the rosbridge with related subscribers and publishers
* rosbridgelib - contains all the specific messages in their respective category with all the added messages in there as well
* WAMSim - the wam sim prefab and all related scripts, including service class, subscribers and publishers for the related topics.

** some scenese and other misc stuff

### Adding Kinect ###
https://github.com/code-iai/iai_kinect2

### Creating Your Own Unity Scene ###

*Use the ROSscene scene asset as a starting point reference. Ensure that under the Publishing Settings tab within Windows Store settings in Player Settings, that the following are checked: "InternetClient" , "InternetClientServer", and "PrivateNetworkClientServer". "Spatial Mapping" can also be checked if you are using the hololens spatial features.  Follow the hololens scene setup as described on https://github.com/Microsoft/HoloToolkit-Unity. Add the WAM prefab to the scene and create your controller script. The WAMViewer script can be used as a starting reference to design of the controller script.

### Creating Your Own ROS topics ###

* Refer to ROS tutorials on what topics and messages are. To create a unity rosbridge topic, look at the WAMSim/scripts folder where any of the scripts aside from WAMViewer is either a publisher or subscriber. 
* To create a custom message, look at any of the ROSBridgeLib messages folder. 
* One example of pairing a custom message and topic would be the JointStageMsg under RosBridgeLib/sensor_msgs/ that is the message being subscribed by the WAMJointState subscriber under WAMSim/Scripts. 

### Who do I talk to? ###

* Repo owner is Cole Shing
* contact at coleshing0603@gmail.com