﻿using UnityEngine;
using System.Collections.Generic;
using ROSBridgeLib;
using ROSBridgeLib.std_msgs;
using System; //for the boolean

/*
 *
 */

public class myo_viewer : MonoBehaviour
{
    public bool sendvibrate;
    public byte value;
    private ROSBridgeWebSocketConnection ros = null; //defined in ROSBridgeWebSocketConnection
    // Define our subscribers, publishers and service response handlers

    void Start()
    {
        //creates the connection to the bridge
        //ros = new ROSBridgeWebSocketConnection("ws://192.168.0.102", 9090); //change to IP of ROS machine        
        ros = new ROSBridgeWebSocketConnection("ws://137.82.173.74", 9090); //change to IP of ROS machine       
        //add subscribers and publishers and services
        ros.AddSubscriber(typeof(myo_gest));
        ros.AddSubscriber(typeof(myo_ori_deg));
        ros.AddPublisher(typeof(vibrate));
        ros.Connect(); //actually connects to the ros bridge
    }

    // When application close, disconnect to ROS bridge
    void OnApplicationQuit()
    {
        if (ros != null)
            ros.Disconnect(); //extremely important to disconnect from ROS.OTherwise packets continue to flow
    }

    // Update is called once per frame in Unity. The Unity camera follows the robot (which is driven by
    // the ROS environment. 
    void Update()
    {

#if UNITY_EDITOR
        if(sendvibrate)
        {
            UInt8Msg msg = new UInt8Msg(value);
            ros.Publish(vibrate.GetMessageTopic(), msg);
            sendvibrate = false;
        }
#endif
        ros.Render(); //pretty much same as ros.spin()

    }
}

