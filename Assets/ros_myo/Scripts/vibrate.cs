﻿using ROSBridgeLib;
using ROSBridgeLib.std_msgs;

/* 
 * written by Cole Shing, 2017
 */
public class vibrate : ROSBridgePublisher
{

    public new static string GetMessageTopic()
    {
        return "/myo_raw/vibrate";
    }

    public new static string GetMessageType()
    {
        return "std_msgs/UInt8";
    }

    public static string ToYAMLString(UInt8Msg msg)
    {
        return msg.ToYAMLString();
    }
}
