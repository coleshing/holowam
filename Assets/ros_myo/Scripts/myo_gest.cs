﻿using ROSBridgeLib;
using SimpleJSON;
using UnityEngine;

/* myo gest subscriber to /myo_raw/myo_gest
* Written by Cole Shing, 2017
*/

public class myo_gest : ROSBridgeSubscriber
{
    static byte pose;

    public new static string GetMessageTopic()
    {
        return "/myo_raw/myo_gest";
    }

    public new static string GetMessageType()
    {
        return "ros_myo/MyoPose";
    }

    public new static ROSBridgeMsg ParseMessage(JSONNode msg)
    {
        return new ROSBridgeLib.myo_msgs.MyoPoseMsg(msg);
    }

    public new static void CallBack(ROSBridgeMsg msg)
    {
        ROSBridgeLib.myo_msgs.MyoPoseMsg myo_pose = (ROSBridgeLib.myo_msgs.MyoPoseMsg)msg;
        pose = myo_pose.GetPose();
#if UNITY_EDITOR
        Debug.Log("pose is " + pose);
#endif
    }
}