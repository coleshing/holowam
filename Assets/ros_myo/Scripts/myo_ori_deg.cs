﻿using ROSBridgeLib;
using SimpleJSON;
using UnityEngine;

/* myo ori subscriber to /myo_raw/myo_ori_deg in degrees 
 * 
    geometry_msgs/Vector3 
        Float64 x
        Float64 y      
        Float64 z

* Written by Cole Shing, 2017
*/

public class myo_ori_deg : ROSBridgeSubscriber
{
    static double x, y, z;
    public new static string GetMessageTopic()
    {
        return "/myo_raw/myo_ori_deg";
    }

    public new static string GetMessageType()
    {
        return "geometry_msgs/Vector3";
    }

    public new static ROSBridgeMsg ParseMessage(JSONNode msg)
    {
        return new ROSBridgeLib.geometry_msgs.Vector3Msg(msg);
    }

    public new static void CallBack(ROSBridgeMsg msg)
    {
        ROSBridgeLib.geometry_msgs.Vector3Msg myo_ori = (ROSBridgeLib.geometry_msgs.Vector3Msg)msg;
#if UNITY_EDITOR
        x = myo_ori.GetX();
        y = myo_ori.GetY();
        z = myo_ori.GetZ();
        Debug.Log("angles are " + x + " " + y + " " + z);
        GameObject.Find("Capsule").transform.localRotation = Quaternion.Euler((float)x, (float)y, (float)z) ;
#endif
    }
}