﻿using ROSBridgeLib;
using SimpleJSON;
using UnityEngine;

/* myo ori subscriber to /myo_raw/myo_ori in radians 
 * 
    geometry_msgs/Vector3 
        Float64 x
        Float64 y      
        Float64 z

* Written by Cole Shing, 2017
*/

public class myo_ori : ROSBridgeSubscriber
{
    public new static string GetMessageTopic()
    {
        return "/myo_raw/myo_ori";
    }

    public new static string GetMessageType()
    {
        return "geometry_msgs/Vector3";
    }

    public new static ROSBridgeMsg ParseMessage(JSONNode msg)
    {
        return new ROSBridgeLib.geometry_msgs.Vector3Msg(msg);
    }

    public new static void CallBack(ROSBridgeMsg msg)
    {
        ROSBridgeLib.geometry_msgs.Vector3Msg myo_ori = (ROSBridgeLib.geometry_msgs.Vector3Msg)msg;
    }
}