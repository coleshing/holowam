﻿using ROSBridgeLib;
using SimpleJSON;
using UnityEngine;

/* myo arm subscriber to /myo_raw/myo_arm
* Written by Cole Shing, 2017
*/

public class myo_arm : ROSBridgeSubscriber
{
    static byte arm;
    static byte xdir;

    public new static string GetMessageTopic()
    {
        return "/myo_raw/myo_arm";
    }

    public new static string GetMessageType()
    {
        return "ros_myo/MyoArm";
    }

    public new static ROSBridgeMsg ParseMessage(JSONNode msg)
    {
        return new ROSBridgeLib.myo_msgs.MyoArmMsg(msg);
    }

    public new static void CallBack(ROSBridgeMsg msg)
    {
        ROSBridgeLib.myo_msgs.MyoArmMsg myo_arm = (ROSBridgeLib.myo_msgs.MyoArmMsg)msg;
        arm = myo_arm.GetArm();
        xdir = myo_arm.GetXDirection();
#if UNITY_EDITOR
        Debug.Log("myo is on " + arm + " arm in " + xdir + " direction");
#endif
    }
}