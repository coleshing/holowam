﻿using ROSBridgeLib;
using SimpleJSON;
using UnityEngine;

/* myo imu subscriber to /myo_raw/myo_imu position is in meter units, 
 * 
    sensor_msgs/Imu
    std_msgs/Header header
        Unit32 seq
        Time stamp
        String frame_id
    geometry_msgs/Quaternion orientation
        Float64 x
        Float64 y      
        Float64 z
        Float64 w
    float64[9] orientation_covariance
    geometry_msgs/Vector3 angular_velocity  //degrees/second
        Float64 x
        Float64 y      
        Float64 z
    float64[9] angular_velocity_covariance
    geometry_msgs/Vector3 linear_acceleration  //in units of g (roughly 9.8 m/s^2)
        Float64 x
        Float64 y      
        Float64 z
    float64[9] linear_acceleration_covariance

* Written by Cole Shing, 2017
*/

public class myo_imu : ROSBridgeSubscriber
{
    static double x, y, z;

    public new static string GetMessageTopic()
    {
        return "/myo_raw/myo_imu";
    }

    public new static string GetMessageType()
    {
        return "sensor_msgs/Imu";
    }

    public new static ROSBridgeMsg ParseMessage(JSONNode msg)
    {
        return new ROSBridgeLib.sensor_msgs.ImuMsg(msg);
    }

    public new static void CallBack(ROSBridgeMsg msg)
    {
        ROSBridgeLib.sensor_msgs.ImuMsg myo_imu = (ROSBridgeLib.sensor_msgs.ImuMsg)msg;
        x = myo_imu.GetOrientation().GetX();
        y = myo_imu.GetOrientation().GetY();
        z = myo_imu.GetOrientation().GetZ();
#if UNITY_EDITOR
        Debug.Log("pose position is : " + x + " " + y + " " + z);
#endif
    }
}