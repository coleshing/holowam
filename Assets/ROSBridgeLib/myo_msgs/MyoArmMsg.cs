﻿using SimpleJSON;

/* 
 * myo msgs MyoArm added ROSBridgeLib, copied from uts-magic-labs
 * Cole Shing, c 2017
 * 
 * uint8 UNKNOWN=0
 * uint8 RIGHT=1
 * uint8 LEFT=2
 * uint8 X_TOWARD_WRIST=11
 * uint8 X_TOWARD_ELBOW=12
 *
 */

namespace ROSBridgeLib
{
    namespace myo_msgs
    {
        public class MyoArmMsg : ROSBridgeMsg
        {
            private byte _arm; //arm used
            private byte _xdir; //x direction

            public MyoArmMsg(JSONNode msg)
            {
                _arm = byte.Parse(msg["arm"]);
                _xdir = byte.Parse(msg["xdir"]);
            }

            public MyoArmMsg(byte arm, byte xdir)
            {
                _arm = arm;
                _xdir = xdir;
            }

            public static string GetMessageType()
            {
                return "ros_myo/MyoArm";
            }

            public byte GetArm()
            {
                return _arm;
            }

            public byte GetXDirection()
            {
                return _xdir;
            }

            public override string ToString()
            {
                return "MyoArm [arm=" + _arm + ", xdir=" + _xdir + "]";
            }

            public override string ToYAMLString()
            {
                return "{\"arm\" : " + _arm + ", \"xdir\" : " + _xdir +  "}";
            }
        }
    }
}