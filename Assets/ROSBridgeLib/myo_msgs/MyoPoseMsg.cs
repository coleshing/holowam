﻿using SimpleJSON;

/* 
 * myo msgs MyoPose added ROSBridgeLib, copied from uts-magic-labs
 * Cole Shing, c 2017
 * 
 * uint8 REST=1
 * uint8 FIST=2
 * uint8 WAVE_IN=3
 * uint8 WAVE_OUT=4
 * uint8 FINGERS_SPREAD=5
 * uint8 THUMB_TO_PINKY=6
 * uint8 UNKNOWN=8
 *
 */

namespace ROSBridgeLib
{
    namespace myo_msgs
    {
        public class MyoPoseMsg : ROSBridgeMsg
        {
            private byte _pose; //current myo pose

            public MyoPoseMsg(JSONNode msg)
            {
                _pose = byte.Parse(msg["pose"]);
            }

            public MyoPoseMsg(byte pose)
            {
                _pose = pose;
            }

            public static string GetMessageType()
            {
                return "ros_myo/MyoPose";
            }

            public byte GetPose()
            {
                return _pose;
            }
            
            public override string ToString()
            {
                return "MyoPose [pose=" + _pose + "]";
            }

            public override string ToYAMLString()
            {
                return "{\"pose\" : " + _pose + "}";
            }
        }
    }
}