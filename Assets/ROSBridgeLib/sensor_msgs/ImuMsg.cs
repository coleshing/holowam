﻿using SimpleJSON;
using ROSBridgeLib.std_msgs;
using ROSBridgeLib.geometry_msgs;

/*  Imu Msg
 *  added to ROSBridgeLib. By Cole Shing, 2017
 *  std_msgs/ Header header
 *  geometry_msgs/Quarternion orientation
 *  float64[9] orientation_covariance
 *  geometry_msgs/Vector3 angular_velocity
 *  float64[9] angular_velocity_covariance
 *  geoemtry_msgs/Vector3 linear_acceleration
 *  float64[9] linear_acceleration_covariance
 */

namespace ROSBridgeLib
{
    namespace sensor_msgs
    {
        public class ImuMsg : ROSBridgeMsg
        {

            ConvertingArraytoString convert = new ConvertingArraytoString();
            private HeaderMsg _header;
            private QuaternionMsg _orientation;
            private double[] _orientation_variance;
            private Vector3Msg _angular_velocity;
            private double[] _angular_velocity_covariance;
            private Vector3Msg _linear_acceleration;
            private double[] _linear_acceleration_covariance;

            public ImuMsg(JSONNode msg)
            {
                _header = new HeaderMsg(msg["header"]);
                _orientation = new QuaternionMsg(msg["orientation"]);
                _orientation_variance = new double[msg["orientation_variance"].Count];
                for (int i = 0; i < _orientation_variance.Length; i++)
                {
                    _orientation_variance[i] = double.Parse(msg["orientation_variance"][i]);
                }
                _angular_velocity = new Vector3Msg(msg["angular_velocity"]);
                _angular_velocity_covariance = new double[msg["angular_velocity_covariance"].Count];
                for (int i = 0; i < _angular_velocity_covariance.Length; i++)
                {
                    _angular_velocity_covariance[i] = double.Parse(msg["angular_velocity_covariance"][i]);
                }
                _linear_acceleration = new Vector3Msg(msg["linear_acceleration"]);
                _linear_acceleration_covariance = new double[msg["linear_acceleration_covariance"].Count];
                for (int i = 0; i < _linear_acceleration_covariance.Length; i++)
                {
                    _linear_acceleration_covariance[i] = double.Parse(msg["linear_acceleration_covariance"][i]);
                }
            }

            public ImuMsg(HeaderMsg header, QuaternionMsg orientation, double[] orientation_variance, 
                Vector3Msg angular_velocity, double[] angular_velocity_covariance, Vector3Msg linear_acceleration,
                double[] linear_acceleration_covariance)
            {
                _header = header;
                _orientation = orientation;
                _orientation_variance = orientation_variance;
                _angular_velocity = angular_velocity;
                _angular_velocity_covariance = angular_velocity_covariance;
                _linear_acceleration = linear_acceleration;
                _linear_acceleration_covariance = linear_acceleration_covariance;
            }

            public static string GetMessageType()
            {
                return "sensor_msgs/Imu";
            }

            public QuaternionMsg GetOrientation()
            {
                return _orientation;
            }

            public double[] GetOrientationCovariance()
            {
                return _orientation_variance;
            }

            public Vector3Msg GetAngularVelocity()
            {
                return _angular_velocity;
            }

            public double[] GetAngularVelocityCovariance()
            {
                return _angular_velocity_covariance;
            }

            public Vector3Msg GetLinearAcceleration()
            {
                return _linear_acceleration;
            }

            public double[] GetLinearAccelerationCovariance()
            {
                return _linear_acceleration_covariance;
            }

            public override string ToString()
            {
                //converting the orientation_variance array into string
                string oriarray = convert.doubletoarray(_orientation_variance);

                //converting the angular_velocity_covariance array into string
                string angvelarray = convert.doubletoarray(_angular_velocity_covariance);

                //converting the linear_acceleration_covariance array into string
                string linarray = convert.doubletoarray(_linear_acceleration_covariance);

                return "Imu [header=" + _header.ToString() + ",  orientation=" + _orientation.ToString() 
                    + ",  orientation_variance=" + oriarray + ",  angular_velocity=" + _angular_velocity.ToString()
                    + ",  angular_velocity_covariance=" + angvelarray + ",  linear_acceleration=" + _linear_acceleration.ToString()
                    + ",  linear_acceleration_covariance=" + linarray + "]";
            }

            public override string ToYAMLString()
            {
                //converting the orientation_variance array into YAML string
                string oriarray = convert.doubletoarray(_orientation_variance);

                //converting the angular_velocity_covariance array into YAML string
                string angvelarray = convert.doubletoarray(_angular_velocity_covariance);

                //converting the linear_acceleration_covariance array into YAML string
                string linarray = convert.doubletoarray(_linear_acceleration_covariance);

                return "{\"header\" : " + _header.ToYAMLString() + ", \"orientation\" : " + _orientation.ToYAMLString() 
                    + ", \"orientation_variance\" : " + oriarray + ", \"angular_velocity\" : " + _angular_velocity.ToYAMLString()
                    + ", \"angular_velocity_covariance\" : " + angvelarray + ", \"linear_acceleration\" : " + _linear_acceleration.ToYAMLString()
                    + ", \"linear_acceleration_covariance\" : " + linarray + "}";
            }
        }
    }
}