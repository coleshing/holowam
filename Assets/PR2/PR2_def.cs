﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


/*
* Joint Type Limit (+) Limit (-)   soft Limit (+)  Limit (-)
∗ caster rotation joint continuous - -
∗ caster wheel ∗ joint continuous - -
torso lift joint prismatic 310 mm 0 mm      0.325f      0.0115f
laser tilt joint revolute 85◦ -45◦          1.43353f    -0.7354f
head pan joint revolute 168◦ -168◦          2.857f      -2.857f
head tilt joint revolute 60◦ -30◦           1.29626f    -0.3712f
r shoulder pan joint revolute 40◦ -130◦     0.5646f     -2.1354f
l shoulder pan joint revolute 130◦ -40◦     2.1354f     -0.5646f
∗ shoulder lift joint revolute 80◦ -30◦     1.2963f     -0.3536f
r upper arm roll joint revolute 44◦ -224◦   0.65f       -3.75f
l upper arm roll joint revolute 224◦ -44◦   0.375f      -0.65f
∗ elbow flex joint revolute 133◦ 0◦         -0.15f      -2.1213f   need to check why it's opposite
∗ forearm roll joint continuous - -                     
∗ wrist flex joint revolute 130◦ 0◦         -2f          -0.1f
∗ wrist roll joint continuous - -
∗ gripper joint prismatic 86 mm 0 mm        0.088f       -0.01f
*/

public class PR2_def: MonoBehaviour {
    private static float continuous = -1f;
    Transform torso_lift_link, head_pan_link, head_tilt_link;
    Transform[] PR2_tf = new Transform[45]; //total transforms from PR2 joint states is 45
    string[] joint_names = {"fl_caster_rotation_joint","fl_caster_l_wheel_joint","fl_caster_r_wheel_joint", "fr_caster_rotation_joint",
                            "fr_caster_l_wheel_joint","fr_caster_r_wheel_joint","bl_caster_rotation_joint","bl_caster_l_wheel_joint",
                            "bl_caster_r_wheel_joint","br_caster_rotation_joint","br_caster_l_wheel_joint","br_caster_r_wheel_joint",
                            "torso_lift_joint", "torso_lift_motor_screw_joint","head_pan_joint","head_tilt_joint","laser_tilt_mount_joint", //index 16
                            "r_upper_arm_roll_joint","r_shoulder_pan_joint","r_shoulder_lift_joint","r_forearm_roll_joint","r_elbow_flex_joint",
                            "r_wrist_flex_joint","r_wrist_roll_joint","r_gripper_joint","r_gripper_l_finger_joint","r_gripper_r_finger_joint",
                            "r_gripper_r_finger_tip_joint","r_gripper_l_finger_tip_joint","r_gripper_motor_screw_joint","r_gripper_motor_slider_joint",//index 30 , end of right arm
                            "l_upper_arm_roll_joint","l_shoulder_pan_joint","l_shoulder_lift_joint","l_forearm_roll_joint","l_elbow_flex_joint",
                            "l_wrist_flex_joint","l_wrist_roll_joint","l_gripper_joint","l_gripper_l_finger_joint","l_gripper_r_finger_joint",
                            "l_gripper_r_finger_tip_joint","l_gripper_l__finger_tip_joint","l_gripper_motor_screw_joint","l_gripper_motor_slider_joint"}; //index 44

    //max and min joint soft limits for PR2, -1 is infinite, taken form pr2_description
    float[] max_lim = { continuous, continuous, continuous, continuous,     //fl_caster rot, fl_caster_l_wheel, fl_caster_r_wheel, fr_caster_rot
                        continuous, continuous, continuous, continuous,     //fr_caster_l_wheel, fr_caster_r_wheel, bl_caster_rot, bl_caster_l_wheel
                        continuous, continuous, continuous, continuous,     //bl_caster_r_wheel, br_caster_rot, br_caster_l_wheel, br_caster_r_wheel
                        0.325f, continuous, 2.857f, 1.29626f, 1.43353f,     //torso_lift, torso_lift_motor_screw, head_pan, head_tilt, laser_tilt_mount   index 16 
                        0.65f, 0.5646f, 1.2963f, continuous, -0.15f,        //r_upper_arm_roll, r_shoulder_pan, r_shoulder_lift, r_forearm_roll, r_elbow_flex
                        -2f, continuous, 0.088f, 0.548f, 0.548f,            //r_wrist_flex, r_wrist_roll, r_gripper, r_gripper_l_finger, r_gripper_r_finger
                        0.548f, 0.548f, 0.548f, 0.548f,                     //r_gripper_r_finger_tip, r_gripper_l_finger_tip, r_gripper_motor_screw, r_gripper_motor_slider  index 30
                        0.375f, 2.1354f, 1.2963f, continuous, -0.15f        //l_upper_arm_roll, l_shoulder_pan, l_shoulder_lift, l_forearm_roll, l_elbow_flex
                        -2f, continuous, 0.088f, 0.548f, 0.548f,            //l_wrist_flex, l_wrist_roll, l_gripper, l_gripper_l_finger, l_gripper_r_finger
                        0.548f, 0.548f, 0.548f, 0.548f };                  //l_gripper_r_finger_tip, l_gripper_l_finger_tip, l_gripper_motor_screw, l_gripper_motor_slider  index 44

    float[] min_lim = { continuous, continuous, continuous, continuous,     //fl_caster rot, fl_caster_l_wheel, fl_caster_r_wheel, fr_caster_rot
                        continuous, continuous, continuous, continuous,     //fr_caster_l_wheel, fr_caster_r_wheel, bl_caster_rot, bl_caster_l_wheel
                        continuous, continuous, continuous, continuous,     //bl_caster_r_wheel, br_caster_rot, br_caster_l_wheel, br_caster_r_wheel
                        0.0115f, continuous, -2.857f, -0.3712f, -0.7354f,   //torso_lift, torso_lift_motor_screw, head_pan, head_tilt, laser_tilt_mount   index 16 
                        -3.75f, -2.1354f, -0.3536f, continuous, -2.1213f,   //r_upper_arm_roll, r_shoulder_pan, r_shoulder_lift, r_forearm_roll, r_elbow_flex
                        -0.1f, continuous, -0.01f, 0.0f, 0.0f,              //r_wrist_flex, r_wrist_roll, r_gripper, r_gripper_l_finger, r_gripper_r_finger
                        0.0f, 0.0f, 0.0f, 0.0f,                             //r_gripper_r_finger_tip, r_gripper_l_finger_tip, r_gripper_motor_screw, r_gripper_motor_slider  index 30
                        -0.65f, -0.5646f, -0.3536f, continuous, -2.1213f    //l_upper_arm_roll, l_shoulder_pan, l_shoulder_lift, l_forearm_roll, l_elbow_flex
                        -0.1f, continuous, -0.01f, 0.0f, 0.0f,              //l_wrist_flex, l_wrist_roll, l_gripper, l_gripper_l_finger, l_gripper_r_finger
                        0.0f, 0.0f, 0.0f, 0.0f };                           //l_gripper_r_finger_tip, l_gripper_l_finger_tip, l_gripper_motor_screw, l_gripper_motor_slider  index 44
  
    public float lift;
    public float turn;
    public float yaw;
    private float torso_height; 
    // Use this for initialization
    void Start () {
        torso_lift_link = transform.Find("torso_lift_link"); //grab the WAM
        head_pan_link = transform.Find("torso_lift_link/head_pan_link");
        head_tilt_link = transform.Find("torso_lift_link/head_pan_link/head_tilt_link");
        if (torso_lift_link == null)
            Debug.Log("torso not found");
        if (head_pan_link == null)
            Debug.Log("head not found");

        torso_height = torso_lift_link.localPosition.z;
    }

    public void Torso_lift_joint(double height) // from 0mm to 310mm , index 12
    {
        if (height > 310)
            height = 310;
        if (height < 0)
            height = 0;

        float distance = System.Convert.ToSingle(height);
        torso_lift_link.localPosition = new Vector3(torso_lift_link.localPosition.x, torso_lift_link.localPosition.y, torso_height + distance/100);
    }

    public void Head_pan_joint(double radians) //from 2.93215 to -2.93215, 168◦ to -168◦, index 14
    {
        if (radians > 2.93215)
            radians = 2.93215;
        if (radians < -2.93215)
            radians = -2.93215;
        float degrees = Mathf.Rad2Deg * System.Convert.ToSingle(radians);
        head_pan_link.localEulerAngles = new Vector3(0, 0, degrees);
    }

    public void Head_tilt_joint(double radians) // from 1.0472 to -0.523599, 60◦ to -30◦, index 15
    {
        if (radians > 1.0472)
            radians = 1.0472;
        if (radians < -0.523599)
            radians = -0.523599;
        float degrees = Mathf.Rad2Deg * System.Convert.ToSingle(radians);
        head_tilt_link.localEulerAngles = new Vector3(0, -degrees, 0);
    }
    //global array of joint names
    //global array of min/max values
    void SetLinkAngle(string joint_name, float joint_value) //set the angle of the link 
    {
       // joint_name

    }

    void rostoindex(double[] name, double[] position)
    {
        //for loop 
        //based on name find corresponding index of joint name array, set position based on index
        //setangletoindex(;
    }
    void Update()
    {

        Torso_lift_joint(lift);
        Head_pan_joint(turn);
        Head_tilt_joint(yaw);
        
    }
}
